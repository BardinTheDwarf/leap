package com.legacy.leap.player.util;

import net.minecraft.world.entity.player.Player;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.event.entity.living.LivingFallEvent;

public interface ILeapPlayer
{
	CompoundTag writeAdditional(CompoundTag nbt);

	void read(CompoundTag nbt);

	boolean hasDoubleJumped();

	void setDoubleJumping(boolean used, double motionX, double motionZ);

	Player getPlayer();

	void onFall(LivingFallEvent event);
}