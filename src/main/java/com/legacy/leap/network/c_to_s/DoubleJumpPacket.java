package com.legacy.leap.network.c_to_s;

import java.util.function.Supplier;

import com.legacy.leap.player.LeapPlayer;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.network.NetworkEvent;

public class DoubleJumpPacket
{
	private final double motionX, motionZ;

	public DoubleJumpPacket(double motionX, double motionZ)
	{
		this.motionX = motionX;
		this.motionZ = motionZ;
	}

	public static void encoder(DoubleJumpPacket packet, FriendlyByteBuf buff)
	{
		buff.writeDouble(packet.motionX);
		buff.writeDouble(packet.motionZ);
	}

	public static DoubleJumpPacket decoder(FriendlyByteBuf buff)
	{
		return new DoubleJumpPacket(buff.readDouble(), buff.readDouble());
	}

	public static void handler(DoubleJumpPacket packet, Supplier<NetworkEvent.Context> context)
	{
		context.get().enqueueWork(() -> handlePacket(packet, context.get().getSender()));
		context.get().setPacketHandled(true);
	}

	private static void handlePacket(DoubleJumpPacket packet, Player player)
	{
		LeapPlayer.ifPresent(player, (leapPlayer) -> leapPlayer.setDoubleJumping(true, packet.motionX, packet.motionZ));
	}
}
