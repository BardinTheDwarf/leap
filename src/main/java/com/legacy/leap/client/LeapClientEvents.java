package com.legacy.leap.client;

import com.legacy.leap.LeapMod;
import com.legacy.leap.network.PacketHandler;
import com.legacy.leap.network.c_to_s.DoubleJumpPacket;

import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.LivingEntity;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.util.ObfuscationReflectionHelper;

public class LeapClientEvents
{
	private static final Minecraft mc = Minecraft.getInstance();

	@SubscribeEvent
	public static void onKeyPressed(InputEvent.KeyInputEvent event)
	{
		if (mc.player == null)
			return;

		if (!LeapMod.Client.LEAP_KEYBIND.isDefault() && LeapMod.Client.LEAP_KEYBIND.isDown())
			PacketHandler.sendToServer(new DoubleJumpPacket(mc.player.getDeltaMovement().x(), mc.player.getDeltaMovement().z()));
	}

	@SubscribeEvent
	public static void onClientTick(ClientTickEvent event)
	{
		if (mc.player == null)
			return;

		// for smoother jumping if space is used, that way you can hold it down
		if (LeapMod.Client.LEAP_KEYBIND.isDefault() && (boolean) ObfuscationReflectionHelper.getPrivateValue(LivingEntity.class, mc.player, "f_20899_"))
			PacketHandler.sendToServer(new DoubleJumpPacket(mc.player.getDeltaMovement().x(), mc.player.getDeltaMovement().z()));
	}
}
