package com.legacy.leap;

import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.entity.EquipmentSlot;

public class LeapingEnchantment extends Enchantment
{
	public LeapingEnchantment(Enchantment.Rarity rarityIn, EquipmentSlot... slots)
	{
		super(rarityIn, EnchantmentCategory.ARMOR_FEET, slots);
	}

	@Override
	public int getMinCost(int enchantmentLevel)
	{
		return enchantmentLevel * 25;
	}

	@Override
	public int getMaxCost(int enchantmentLevel)
	{
		return this.getMinCost(enchantmentLevel) + 50;
	}

	@Override
	public boolean isTreasureOnly()
	{
		return true;
	}

	@Override
	public int getMaxLevel()
	{
		return 1;
	}

	@Override
	public boolean checkCompatibility(Enchantment ench)
	{
		return super.checkCompatibility(ench) && ench != Enchantments.DEPTH_STRIDER && ench != Enchantments.FROST_WALKER && !ench.getRegistryName().toString().contains("stepping");
	}
}